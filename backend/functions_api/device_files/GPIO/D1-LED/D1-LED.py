from machine import Pin
import time
from umqtt.simple import MQTTClient

# Configuration settings
SERVER = "brokerip"
CLIENT_ID = "unique_client_id"
TOPIC = b"home/room/led"

# Setup the LED
led_num = 10
led = Pin(led_num, Pin.OUT)

# Define the MQTT callback function
def mqtt_callback(topic, msg):
    print("Received message:", msg)
    if msg == b"on":
        led.value(1)
    elif msg == b"off":
        led.value(0)

# Setup MQTT client
client = MQTTClient(CLIENT_ID, SERVER)
client.set_callback(mqtt_callback)
client.connect()
client.subscribe(TOPIC)

print("Connected to %s, subscribed to %s topic" % (SERVER, TOPIC))

try:
    while True:
        client.check_msg()  # Check for new messages on subscription
        time.sleep(0.1)  # Sleep to allow for context switching
finally:
    client.disconnect()




# from machine import Pin
# import time
# led_num = 10
# led = Pin(led_num,Pin.OUT)
# print("led demo")
# while True:
#     led.toggle()
#     time.sleep_ms(500)
                
