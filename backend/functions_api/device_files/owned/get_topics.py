import json
from umqtt.simple import MQTTClient

# Configuration settings
MQTT_BROKER = 'HOST_ADDRESS'
CLIENT_ID = 'ESP32_client'
CONTROL_TOPIC = 'esp32/control'

def connect_to_mqtt():
    """ Connects to the MQTT broker and returns the client. """
    client = MQTTClient(CLIENT_ID, MQTT_BROKER)
    client.connect()
    return client

def subscribe_to_topic(client, topic):
    """ Subscribes to a specific topic. """
    client.subscribe(topic)

def on_message(client, topic, message):
    """ Handles incoming messages on subscribed topics. """
    print(f"Received message on topic {topic}: {message}")
    if topic == CONTROL_TOPIC.encode():
        try:
            data = json.loads(message)
            if 'subscribe' in data:
                for sub_topic in data['subscribe']:
                    subscribe_to_topic(client, sub_topic.encode())
                    print(f"Subscribed to {sub_topic}")
        except json.JSONDecodeError:
            print("Error decoding JSON.")

def main():
    client = connect_to_mqtt()
    subscribe_to_topic(client, CONTROL_TOPIC)
    client.set_callback(on_message)
    client.loop_forever()

if __name__ == '__main__':
    main()

