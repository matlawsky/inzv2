import unittest
from unittest.mock import patch, MagicMock
import sys
import glob
import logging
import serial
import serial_communication

class TestSerialPorts(unittest.TestCase):

    @patch('serial_communication.glob.glob')
    @patch('serial_communication.serial.Serial')
    @patch('serial_communication.sys.platform', 'linux')
    def test_serial_ports_linux(self, mock_serial, mock_glob):
        # Simulate Linux environment
        mock_glob.return_value = ['/dev/ttyUSB0', '/dev/ttyUSB1']
        mock_serial.side_effect = lambda x: MagicMock(close=lambda: None)

        result = serial_communication.serial_ports()
        self.assertEqual(result, ['/dev/ttyUSB0', '/dev/ttyUSB1'])
        mock_glob.assert_called_with('/dev/tty[A-Za-z]*')

    @patch('serial_communication.serial.Serial')
    @patch('serial_communication.sys.platform', 'win32')
    def test_serial_ports_windows(self, mock_serial):
        # Simulate Windows environment
        mock_serial.side_effect = lambda x: MagicMock(close=lambda: None) if x in ['COM3', 'COM4'] else serial.SerialException()

        result = serial_communication.serial_ports()
        self.assertIn('COM3', result)
        self.assertIn('COM4', result)

    @patch('serial_communication.sys.platform', 'unsupported_os')
    def test_serial_ports_unsupported_os(self):
        # Test for unsupported OS
        with self.assertRaises(EnvironmentError):
            serial_communication.serial_ports()

class TestDeviceOperations(unittest.TestCase):
    # viable_port = serial_communication.serial_ports()[0]
    
    @patch('serial_communication.os.system')
    def test_run_file_success(self, mock_system):
        """
        Test running a file on the device successfully.
        """
        mock_system.return_value = 0  # Simulate successful execution
        result = serial_communication.run_file('COM3', 'path/to/file.py')
        mock_system.assert_called_with('ampy --port COM3 run --no-output path/to/file.py')
        self.assertEqual(result, 1)

    @patch('serial_communication.os.system')
    def test_run_file_failure(self, mock_system):
        """
        Test running a file on the device with an error.
        """
        mock_system.side_effect = Exception('Error')  # Simulate an exception
        result = serial_communication.run_file('COM3', 'path/to/file.py')
        mock_system.assert_called_with('ampy --port COM3 run --no-output path/to/file.py')
        self.assertEqual(result, False)

    @patch('serial_communication.os.system')
    def test_put_file_success(self, mock_system):
        """
        Test putting files on a device successfully.
        """
        mock_system.return_value = 0  # Simulate successful execution
        result = serial_communication.put_file('COM3', ['file1.txt', 'file2.txt'])
        mock_system.assert_called_with('ampy --port COM3 put --no-output file1.txt file2.txt')
        self.assertIsNone(result)  # The original function does not return anything on success

    @patch('serial_communication.os.system')
    def test_put_file_failure(self, mock_system):
        """
        Test failure in putting files on a device.
        """
        mock_system.side_effect = Exception('Error')  # Simulate an exception
        result = serial_communication.put_file('COM3', ['file1.txt', 'file2.txt'])
        mock_system.assert_called_with('ampy --port COM3 put --no-output file1.txt file2.txt')
        self.assertEqual(result, False)

    @patch('serial_communication.os.system')
    def test_copy_file_success(self, mock_system):
        """
        Test copying a file from a device successfully.
        """
        mock_system.return_value = 0  # Simulate successful execution
        result = serial_communication.copy_file_from_device('COM3', 'source_path', 'destination_path')
        mock_system.assert_called_with('ampy --port COM3 get source_path destination_path')
        self.assertEqual(result, 1)

    @patch('serial_communication.os.system')
    def test_copy_file_failure(self, mock_system):
        """
        Test copying a file from a device with an error.
        """
        mock_system.side_effect = Exception('Error')  # Simulate an exception
        result = serial_communication.copy_file_from_device('COM3', 'source_path', 'destination_path')
        mock_system.assert_called_with('ampy --port COM3 get source_path destination_path')
        self.assertEqual(result, False)

    @patch('serial_communication.os.system')
    def test_make_directory_on_device_success(self, mock_system):
        mock_system.return_value = 0  # Simulate successful execution
        result = serial_communication.make_directory_on_device('COM3', 'new_directory')
        mock_system.assert_called_with('ampy --port COM3 mkdir new_directory')
        self.assertEqual(result, 1)

    @patch('serial_communication.os.system')
    def test_make_directory_on_device_exception(self, mock_system):
        mock_system.side_effect = Exception('Error')  # Simulate an exception
        result = serial_communication.make_directory_on_device('COM3', 'new_directory')
        self.assertEqual(result, False)

    @patch('serial_communication.os.popen')
    def test_list_files_on_device_success(self, mock_popen):
        mock_popen.return_value.read.return_value = "file1\nfile2\n"
        result = serial_communication.list_files_on_device('COM3', 'some_directory')
        mock_popen.assert_called_with('ampy --port COM3 ls some_directory')
        self.assertEqual(result, ['file1', 'file2'])

    @patch('serial_communication.os.popen')
    def test_list_files_on_device_exception(self, mock_popen):
        mock_popen.side_effect = Exception('Error')  # Simulate an exception
        result = serial_communication.list_files_on_device('COM3', 'some_directory')
        self.assertEqual(result, False)

    @patch('serial_communication.os.system')
    def test_remove_files_on_device_success(self, mock_system):
        """
        Test removing files from a device successfully.
        """
        mock_system.return_value = 0  # Simulate successful command execution
        result = serial_communication.remove_files_on_device('COM3', 'path/to/file')
        mock_system.assert_called_with('ampy --port COM3 rm path/to/file')
        self.assertEqual(result, 1)

    @patch('serial_communication.os.system')
    def test_remove_files_on_device_exception(self, mock_system):
        """
        Test handling of an exception when removing files from a device.
        """
        mock_system.side_effect = Exception('Error')  # Simulate an exception
        result = serial_communication.remove_files_on_device('COM3', 'path/to/file')
        self.assertEqual(result, False)

    @patch('serial_communication.os.system')
    def test_remove_dir_on_device_success(self, mock_system):
        """
        Test removing directories from a device successfully.
        """
        mock_system.return_value = 0  # Simulate successful command execution
        result = serial_communication.remove_dir_on_device('COM3', 'path/to/dir')
        mock_system.assert_called_with('ampy --port COM3 rm path/to/dir')
        self.assertEqual(result, 1)

    @patch('serial_communication.os.system')
    def test_remove_dir_on_device_exception(self, mock_system):
        """
        Test handling of an exception when removing directories from a device.
        """
        mock_system.side_effect = Exception('Error')  # Simulate an exception
        result = serial_communication.remove_dir_on_device('COM3', 'path/to/dir')
        self.assertEqual(result, False)

if __name__ == '__main__':
    unittest.main()