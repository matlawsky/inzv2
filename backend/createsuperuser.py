from django.db import IntegrityError
from django.contrib.auth.models import User

try:
    superuser = User.objects.create_superuser(
        username='MATEO',
        email='matlawsky@gmail.com',
        password='MATEO', default="abc")
    superuser.save()
except IntegrityError:
    print(f"Super User with username MATEO is already exit!")
except Exception as e:
    print(e)