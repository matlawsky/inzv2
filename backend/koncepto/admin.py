from django.contrib import admin
from .models import Device, DataPoint, CodeFile, Topic

# admin.site.register(Project)
admin.site.register(Device)
# admin.site.register(UserViewFile)
admin.site.register(CodeFile)
admin.site.register(DataPoint)
admin.site.register(Topic)