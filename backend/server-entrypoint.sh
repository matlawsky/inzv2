#!/bin/sh
python manage.py makemigrations users --noinput
python manage.py makemigrations koncepto  --noinput
python manage.py makemigrations --noinput
python manage.py migrate  --noinput
python manage.py collectstatic --noinput
python manage.py create_superuser
python manage.py subscribe_mqtt &
python manage.py runserver 0.0.0.0:8000
# gunicorn backend.wsgi --bind 0.0.0.0:8000 --log-level "debug" --workers 2
