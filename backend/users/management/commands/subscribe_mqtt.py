from django.core.management.base import BaseCommand
from paho.mqtt import client as mqtt_client
from django.db import transaction
from django.conf import settings
from koncepto.models import Topic, DataPoint
import json
from decouple import config
import os

class Command(BaseCommand):
    help = 'Command to subscribe to MQTT topics and store received messages in the database.'

    def add_arguments(self, parser):
        parser.add_argument('--service-mode', action='store_true', help="Run the MQTT client in a non-blocking mode")

    def handle(self, *args, **options):
        service_mode = options.get('service_mode', False)

        def on_connect(client, userdata, flags, rc, properties):
            if rc == 0:
                self.stdout.write(self.style.SUCCESS('Connected to MQTT Broker!'))
                topics = Topic.objects.filter(subscribed=True)
                for topic in topics:
                    client.subscribe(topic.name)
                    self.stdout.write(self.style.SUCCESS(f'Subscribed to {topic.name}'))
            else:
                self.stdout.write(self.style.ERROR('Failed to connect, return code %d\n', rc))

        def on_message(client, userdata, msg):
            with transaction.atomic():
                try:
                    topic_name = msg.topic
                    data = json.loads(msg.payload.decode())
                    topic_obj, created = Topic.objects.get_or_create(name=topic_name)
                    DataPoint.objects.create(topic=topic_obj, data=data)
                    self.stdout.write(self.style.SUCCESS(f'Message received on {topic_name}: {data}'))
                except json.JSONDecodeError:
                    self.stdout.write(self.style.ERROR('Failed to decode JSON data'))

        mytransport = 'tcp'
        client = mqtt_client.Client(client_id="myPy", transport=mytransport, protocol=mqtt_client.MQTTv5)
        client.tls_set(ca_certs="/usr/src/certs/ca/ca.crt", certfile="/usr/src/certs/client/client.crt", keyfile="/usr/src/certs/client/client.key")
        client.on_connect = on_connect
        client.on_message = on_message
        client.username_pw_set("mateo", "mateo")

        client.connect("mqtt5", 8883, keepalive=60)

        if service_mode:
            client.loop_start()  # Run the loop in a separate thread
            self.stdout.write(self.style.SUCCESS('MQTT client is running in service mode.'))
            import time
            while True:  # Keep the main thread alive to handle signals properly
                time.sleep(1)
        else:
            client.loop_forever()  # Blocking call