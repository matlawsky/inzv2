import os
from django.core.management.base import BaseCommand
from django.contrib.auth import get_user_model


class Command(BaseCommand):
    help = "Creates a superuser from environment variables"

    def handle(self, *args, **options):
        User = get_user_model()
        username = os.getenv("DJANGO_SUPERUSER_USERNAME")
        email = os.getenv("DJANGO_SUPERUSER_EMAIL")
        password = os.getenv("DJANGO_SUPERUSER_PASSWORD")

        self.stdout.write(
            f"Attempting to create superuser with username: {username}, email: {email}"
        )
        try:
            if username and email and password:
                if not User.objects.filter(username=username).exists():
                    User.objects.create_superuser(username, email, password)
                    self.stdout.write(
                        self.style.SUCCESS("Successfully created superuser")
                    )
                else:
                    self.stdout.write(self.style.SUCCESS("Superuser already exists"))
            else:
                self.stdout.write(
                    self.style.ERROR("Environment variables not set correctly")
                )
                # self.stdout.write(self.style.SUCCESS('Successfully created new super user'))
        except:
            self.stdout.write(self.style.ERROR("This user already exist."))
