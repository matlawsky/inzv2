from django.contrib.auth import get_user_model
from django.test import TestCase

class CustomUserTests(TestCase):
    """
    Custom user test
    """
    def test_create_user(self):
        """
        New simple user
        """
        User = get_user_model()
        user = User.objects.create_user( # type: ignore
            username="matt",
            email="matt@email.com",
            password="testpass123"
        )
        self.assertEqual(user.username, "matt")
        self.assertEqual(user.email, "matt@email.com")
        self.assertTrue(user.is_active)
        self.assertFalse(user.is_staff)
        self.assertFalse(user.is_superuser)

    def test_create_superuser(self):
        """
        New super user
        """
        User = get_user_model()
        admin_user = User.objects.create_superuser( # type: ignore
            username="superadmin",
            email="superadmin@email.com",
            password="testpass123"
        )
        self.assertEqual(admin_user.username, "superadmin")
        self.assertEqual(admin_user.email, "superadmin@email.com")
        self.assertTrue(admin_user.is_active)
        self.assertTrue(admin_user.is_staff)
        self.assertTrue(admin_user.is_superuser)
