import 'package:flutter/material.dart';
import 'package:frontend/mqtt_widgets/mqtt_service.dart';
import 'package:frontend/provider/auth_provider.dart';
import 'package:frontend/screens/code_creator_screen.dart';
import 'package:frontend/screens/login_screen.dart';
import 'package:frontend/screens/devices_screen.dart';
import 'package:frontend/screens/register_screen.dart';
import 'screens/main_screen.dart';
import 'provider/color_state.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => AuthProvider()),
        ChangeNotifierProvider(create: (_) => ColorState()),
        ChangeNotifierProvider(create: (_) => MQTTManager()),
      ],
      child: MaterialApp(
          title: 'Koncepto',
          theme: ThemeData(
            primarySwatch: Colors.blue,
            visualDensity: VisualDensity.adaptivePlatformDensity,
          ),
          home: LoginScreen(), // Set the initial route to login screen
          routes: {
            '/login': (context) => LoginScreen(),
            '/register': (context) => RegisterScreen(),
            '/home': (context) => MainScreen(),
            '/code': (context) => const CodeCreatorScreen(),
            '/devices': (context) => DevicesScreen(),
          }),
    );
  }
}
