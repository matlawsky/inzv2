import 'package:flutter/material.dart';
import 'package:frontend/provider/auth_provider.dart';
import 'package:frontend/screens/create_edit_device_screen.dart';
import 'package:frontend/screens/device_screen.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:provider/provider.dart';
import 'package:frontend/provider/color_state.dart';

class DevicesScreen extends StatefulWidget {
  @override
  DevicesScreenState createState() => DevicesScreenState();
}

class DevicesScreenState extends State<DevicesScreen> {
  List<dynamic> _devices = [];
  List<dynamic> _filteredDevices = [];
  bool _isLoading = true;

  @override
  void initState() {
    super.initState();
    _fetchDevices();
  }

  Future<void> _fetchDevices() async {
    try {
      var authProvider = Provider.of<AuthProvider>(context, listen: false);
      var response = await http.get(
        Uri.parse('http://127.0.0.1:8000/api/devices/'),
        headers: {'Authorization': 'Bearer ${authProvider.token}'},
      );

      if (response.statusCode == 200) {
        setState(() {
          _devices = json.decode(response.body);
          _filteredDevices = _devices;
          _isLoading = false;
        });
      } else if (response.statusCode == 401) {
        // If unauthorized, attempt token refresh
        bool tokenRefreshed = await authProvider.refreshAccessToken();

        if (tokenRefreshed && mounted) {
          // If token is refreshed and the widget is still in the tree, retry fetching devices
          _fetchDevices();
        } else {
          // Handle failed token refresh or unmounted widget
          // You might want to navigate to the login screen or show an error message
        }
      } else {
        print(
            'Failed to load devices. Status code: ${response.statusCode}'); // Debugging information
        setState(() {
          _isLoading = false;
        });
      }
    } catch (e, stacktrace) {
      print('Exception caught: $e');
      print('Stacktrace: $stacktrace');
      if (mounted) {
        setState(() {
          _isLoading = false;
        });
      }
    }
  }

  void _navigateToEditDevice(dynamic device) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => CreateEditDeviceScreen(
          args: DeviceScreenArguments(device: device),
        ),
      ),
    );
  }

  Widget _buildDeviceItem(dynamic device) {
    return ListTile(
      onTap: () {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => DeviceDetailScreen(
                deviceId: device['id']), // Pass the device ID
          ),
        );
      },
      title: Text(device['name']),
      trailing: IconButton(
        icon: const Icon(Icons.edit),
        onPressed: () => _navigateToEditDevice(device),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final colorState = Provider.of<ColorState>(context);
    return Scaffold(
      appBar: AppBar(
        title: const Text("Devices"),
        backgroundColor: colorState.mainColor,
        // actions: [
        //   // TODO: Zrobić system dla TODO fetch
        //   // IconButton(
        //   //   icon: Icon(Icons.search),
        //   //   onPressed: () {
        //   //     showSearch(
        //   //         context: context,
        //   //         delegate:
        //   //             DevicesSearchDelegate(_devices, _filterDevices));
        //   //   },
        //   // ),
        // ],
      ),
      body: _isLoading
          ? const Center(child: CircularProgressIndicator())
          : ListView.builder(
              itemCount: _filteredDevices.length,
              itemBuilder: (context, index) {
                return _buildDeviceItem(_filteredDevices[index]);
              },
            ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) => CreateEditDeviceScreen(
                args: DeviceScreenArguments(), // For creating a new device
              ),
            ),
          );
        },
        tooltip: 'Create Device',
        child: const Icon(Icons.add),
      ),
    );
  }
}

class DevicesSearchDelegate extends SearchDelegate {
  final List<dynamic> devices;
  final Function(String) filterDevices;

  DevicesSearchDelegate(this.devices, this.filterDevices);

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        icon: const Icon(Icons.clear),
        onPressed: () {
          query = '';
          _updateSearchResults();
        },
      ),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: const Icon(Icons.arrow_back),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    _updateSearchResults();
    return Container();
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    _updateSearchResults();
    return Container();
  }

  void _updateSearchResults() {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      filterDevices(query);
    });
  }
}
