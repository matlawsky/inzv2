import 'dart:convert';
import 'package:frontend/provider/auth_provider.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:mqtt_client/mqtt_client.dart' as mqtt;
import 'package:mqtt_client/mqtt_browser_client.dart' as mqtt;
import 'package:fl_chart/fl_chart.dart';
import 'package:provider/provider.dart';

class MQTTDataTableScreen extends StatefulWidget {
  final String mqttServer;
  final String mqttClientId;
  final String mqttTopic;
  final int? deviceId;

  const MQTTDataTableScreen(
      {Key? key,
      required this.mqttServer,
      required this.mqttClientId,
      this.mqttTopic = "",
      required this.deviceId})
      : super(key: key);

  @override
  _MQTTDataTableScreenState createState() => _MQTTDataTableScreenState();
}

class _MQTTDataTableScreenState extends State<MQTTDataTableScreen>
    with SingleTickerProviderStateMixin {
  final _mqttServerController = TextEditingController();
  final _mqttTopicController = TextEditingController();
  final _messageController = TextEditingController();
  mqtt.MqttBrowserClient? _client;
  final List<List<dynamic>> _messages = [];
  List<FlSpot> _dataPoints = [];
  late TabController _tabController;

  @override
  void initState() {
    super.initState();

    _tabController = TabController(length: 2, vsync: this);
    // ensureTopicSubscription(_mqttTopicController.text);
  }

  @override
  void dispose() {
    _mqttServerController.dispose();
    _mqttTopicController.dispose();
    _messageController.dispose();
    _client?.disconnect();
    _tabController.dispose();
    super.dispose();
  }

  Future<void> _assignTopicToDevice(String topicId) async {
    var authProvider = Provider.of<AuthProvider>(context, listen: false);
    final response = await http.patch(
      Uri.parse('http://127.0.0.1:8000/api/topic/'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer ${authProvider.token}',
      },
      body: jsonEncode(<String, String>{
        'topic_id': topicId,
      }),
    );

    if (response.statusCode == 200) {
      print("Topic successfully assigned to device!");
    } else {
      print("Failed to assign topic to device: ${response.body}");
    }
  }

  Future<void> _addDataPoint(String topicId, var data) async {
    var authProvider = Provider.of<AuthProvider>(context, listen: false);
    final response = await http.post(
      Uri.parse('http://127.0.0.1:8000/api/datapoints/'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer ${authProvider.token}',
      },
      body: jsonEncode(<String, String>{'topic_id': topicId, "data": data}),
    );

    if (response.statusCode == 200) {
      print("Topic successfully assigned to device!");
    } else {
      print("Failed to assign topic to device: ${response.body}");
    }
  }

  // // Function to handle the creation or update of a topic
  // Future<void> ensureTopicSubscription(String topic) async {
  //   var authProvider = Provider.of<AuthProvider>(context, listen: false);
  //   final response = await http.get(
  //     Uri.parse('http://127.0.0.1:8000/api/topic/$topic/'),
  //     headers: <String, String>{
  //       'Content-Type': 'application/json; charset=UTF-8',
  //       'Authorization': 'Bearer ${authProvider.token}',
  //     },
  //   );
  //   if (response.statusCode == 200) {
  //     var data = jsonDecode(response.body);
  //     if (data.isEmpty) {
  //       // No such topic, create one
  //       await _createTopic(topic);
  //     } else if (!(data[0]['subscribed'] as bool)) {
  //       // Topic exists but not subscribed
  //       await _updateSubscriptionStatus(data[0]['id'], true);
  //     }
  //   }
  // }

  Future<void> _createTopic(String topic) async {
    var authProvider = Provider.of<AuthProvider>(context, listen: false);
    final response = await http.post(
      Uri.parse('http://127.0.0.1:8000/api/topic/'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer ${authProvider.token}',
      },
      body: jsonEncode(<String, dynamic>{
        'name': topic,
        'subscribed': true,
      }),
    );

    if (response.statusCode == 201) {
      print("Topic created successfully!");
      var data = jsonDecode(response.body);
      String topicId = data['id'];
      await _assignTopicToDevice(topicId); // Assign the topic to the device
    } else {
      print("Failed to create topic: ${response.body}");
    }
  }

  Future<void> _updateSubscriptionStatus(
      String topicName, bool subscribed) async {
    var authProvider = Provider.of<AuthProvider>(context, listen: false);
    final response = await http.patch(
      Uri.parse('http://127.0.0.1:8000/api/topic/$topicName/'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer ${authProvider.token}',
      },
      body: jsonEncode(<String, bool>{
        'subscribed': subscribed,
      }),
    );
    if (response.statusCode == 200) {
      print("Subscription status updated successfully!");
    } else {
      print("Failed to update subscription status: ${response.body}");
    }
  }

  void _connectAndSubscribe() async {
    String server = _mqttServerController.text.isNotEmpty
        ? _mqttServerController.text
        : "ws://127.0.0.1/";
    String topic = _mqttTopicController.text;
    await _createTopic(topic);
    // await
    _client = mqtt.MqttBrowserClient.withPort(server, '', 9001);
    _client!.logging(on: false);
    _client!.websocketProtocols =
        mqtt.MqttClientConstants.protocolsSingleDefault;
    _client!.setProtocolV311();
    _client!.onConnected = _onConnected;
    _client!.onDisconnected = _onDisconnected;
    _client!.onSubscribed = _onSubscribed;

    try {
      await _client!.connect();
    } catch (e) {
      print('Error: $e');
    }
  }

  void _onConnected() {
    print('Connected to MQTT');
    _client!.subscribe(_mqttTopicController.text, mqtt.MqttQos.atMostOnce);
  }

  void _onDisconnected() {
    print('Disconnected from MQTT');
  }

  void _onSubscribed(String topic) {
    print('Subscribed to topic: $topic');
    _client!.updates!
        .listen((List<mqtt.MqttReceivedMessage<mqtt.MqttMessage>> c) {
      final mqtt.MqttPublishMessage recMess =
          c[0].payload as mqtt.MqttPublishMessage;
      final String message = mqtt.MqttPublishPayload.bytesToStringAsString(
          recMess.payload.message);

      setState(() {
        _messages.add([DateTime.now().toString(), topic, message]);
        _addDataPoint(topic, message);
        _dataPoints.add(
            FlSpot(_messages.length.toDouble(), double.tryParse(message) ?? 0));
      });
    });
  }

  void _publishMessage() {
    if (_client?.connectionStatus?.state ==
        mqtt.MqttConnectionState.connected) {
      final String message = _messageController.text;
      if (message.isNotEmpty) {
        final mqtt.MqttClientPayloadBuilder builder =
            mqtt.MqttClientPayloadBuilder();
        builder.addString(message);

        _client?.publishMessage(_mqttTopicController.text,
            mqtt.MqttQos.atMostOnce, builder.payload!);
        print('Message published: $message');
      } else {
        print('Message is empty, not published');
      }
    } else {
      print('Client is not connected, cannot publish message');
    }
  }

  Widget _buildDataTable() {
    return DataTable(
      columns: const <DataColumn>[
        DataColumn(label: Text('Time')),
        DataColumn(label: Text('Topic')),
        DataColumn(label: Text('Message')),
      ],
      rows: _messages
          .map<DataRow>(
            (message) => DataRow(
              cells: <DataCell>[
                DataCell(Text(message[0])),
                DataCell(Text(message[1])),
                DataCell(Text(message[2])),
              ],
            ),
          )
          .toList(),
    );
  }

  Widget _buildGraph() {
    return SizedBox(
      height: 200,
      child: LineChart(
        LineChartData(
          gridData: FlGridData(show: false),
          borderData: FlBorderData(show: true),
          titlesData: FlTitlesData(show: true),
          lineBarsData: [
            LineChartBarData(
              spots: _dataPoints,
              isCurved: true,
              barWidth: 2,
              color: Colors.blue,
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('MQTT Data Table'),
        bottom: TabBar(
          controller: _tabController,
          tabs: const [
            Tab(text: 'Subscribe'),
            Tab(text: 'Publish'),
          ],
        ),
      ),
      body: TabBarView(
        controller: _tabController,
        children: [
          SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 24.0,
                    vertical: 10,
                  ),
                  child: TextField(
                    controller: _mqttTopicController,
                    decoration: const InputDecoration(labelText: 'MQTT Topic'),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 24.0,
                    vertical: 10,
                  ),
                  child: ElevatedButton(
                    onPressed: _connectAndSubscribe,
                    child: const Text('Connect and Subscribe'),
                  ),
                ),
                _buildGraph(),
                _buildDataTable(),
              ],
            ),
          ),
          SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextField(
                    controller: _mqttServerController,
                    decoration: const InputDecoration(labelText: 'MQTT Topic'),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextField(
                    controller: _messageController,
                    decoration:
                        const InputDecoration(labelText: 'Message to Publish'),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ElevatedButton(
                    onPressed: () async {
                      _publishMessage;
                      await _createTopic(_mqttServerController.text);
                    },
                    child: const Text('Publish Message'),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}

// import 'dart:convert';
// import 'package:flutter/material.dart';
// import 'package:http/http.dart' as http;
// import 'package:mqtt_client/mqtt_client.dart' as mqtt;
// import 'package:mqtt_client/mqtt_browser_client.dart' as mqtt;
// import 'package:fl_chart/fl_chart.dart';

// class MQTTDataTableScreen extends StatefulWidget {
//   final String mqttServer;
//   final String mqttClientId;
//   final String mqttTopic;

//   const MQTTDataTableScreen({
//     Key? key,
//     required this.mqttServer,
//     required this.mqttClientId,
//     this.mqttTopic = "",
//   }) : super(key: key);

//   @override
//   _MQTTDataTableScreenState createState() => _MQTTDataTableScreenState();
// }

// class _MQTTDataTableScreenState extends State<MQTTDataTableScreen> with SingleTickerProviderStateMixin {
//   final _mqttServerController = TextEditingController();
//   final _mqttTopicController = TextEditingController();
//   final _messageController = TextEditingController();
//   mqtt.MqttBrowserClient? _client;
//   final List<List<dynamic>> _messages = [];
//   List<FlSpot> _dataPoints = [];
//   late TabController _tabController;

//   @override
//   void initState() {
//     super.initState();
//     _tabController = TabController(length: 2, vsync: this);
//   }

//   @override
//   void dispose() {
//     _mqttServerController.dispose();
//     _mqttTopicController.dispose();
//     _messageController.dispose();
//     _client?.disconnect();
//     _tabController.dispose();
//     super.dispose();
//   }

//   Future<void> ensureTopicSubscription(String topic) async {
//     // Handling topic subscription
//   }

//   Future<void> _createTopic(String topic) async {
//     // Create a topic
//   }

//   Future<void> _updateSubscriptionStatus(String topicName, bool subscribed) async {
//     // Update subscription status
//   }

//   void _connectAndSubscribe() {
//     // Handle MQTT connection and subscription
//   }

//   Widget _buildDataTable() {
//     return DataTable(
//       columns: const <DataColumn>[
//         DataColumn(label: Text('Time')),
//         DataColumn(label: Text('Topic')),
//         DataColumn(label: Text('Message')),
//       ],
//       rows: _messages.map<DataRow>((message) => DataRow(cells: <DataCell>[
//         DataCell(Text(message[0])),
//         DataCell(Text(message[1])),
//         DataCell(Text(message[2])),
//       ])).toList(),
//     );
//   }

//   Widget _buildGraph() {
//     return SizedBox(
//       height: 250, // Increased height
//       child: LineChart(
//         LineChartData(
//           minY: 0,
//           maxY: 100, // Adjusted for the expected range of MQTT data
//           gridData: FlGridData(show: true, drawVerticalLine: true),
//           borderData: FlBorderData(show: true, border: Border.all(color: Colors.blueAccent, width: 2)),
//           titlesData: FlTitlesData(show: true),
//           lineBarsData: [
//             LineChartBarData(
//               spots: _dataPoints,
//               isCurved: true,
//               colors: [Colors.redAccent],
//               barWidth: 5, // Thicker bar for visibility
//               dotData: FlDotData(show: true),
//               belowBarData: BarAreaData(show: true, colors: [Colors.redAccent.withOpacity(0.3)]),
//             ),
//           ],
//         ),
//       ),
//     );
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: const Text('MQTT Data Table'),
//         bottom: TabBar(
//           controller: _tabController,
//           tabs: const [Tab(text: 'Subscribe'), Tab(text: 'Publish')],
//         ),
//       ),
//       body: TabBarView(
//         controller: _tabController,
//         children: [
//           SingleChildScrollView(
//             child: Column(
//               children: <Widget>[
//                 Padding(
//                   padding: const EdgeInsets.all(8.0),
//                   child: TextField(
//                     controller: _mqttTopicController,
//                     decoration: const InputDecoration(labelText: 'MQTT Topic'),
//                   ),
//                 ),
//                 ElevatedButton(
//                   onPressed: _connectAndSubscribe,
//                   child: const Text('Connect and Subscribe'),
//                 ),
//                 _buildGraph(),
//                 _buildDataTable(),
//               ],
//             ),
//           ),
//           SingleChildScrollView(
//             child: Column(
//               children: <Widget>[
//                 Padding(
//                   padding: const EdgeInsets.all(8.0),
//                   child: TextField(
//                     controller: _mqttServerController,
//                     decoration: const InputDecoration(labelText: 'MQTT Server'),
//                   ),
//                 ),
//                 Padding(
//                   padding: const EdgeInsets.all(8.0),
//                   child: TextField(
//                     controller: _messageController,
//                     decoration: const InputDecoration(labelText: 'Message to Publish'),
//                   ),
//                 ),
//                 ElevatedButton(
//                   onPressed: () => _publishMessage(),
//                   child: const Text('Publish Message'),
//                 ),
//               ],
//             ),
//           ),
//         ],
//       ),
//     );
//   }

//   void _publishMessage() {
//     // Publish a message
//   }
// }
