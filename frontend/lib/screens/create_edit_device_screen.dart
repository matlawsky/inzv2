import 'package:flutter/material.dart';
import 'package:frontend/provider/auth_provider.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:provider/provider.dart';
import 'package:frontend/provider/auth_provider.dart';

class DeviceScreenArguments {
  final dynamic device;

  DeviceScreenArguments({this.device});
}

class CreateEditDeviceScreen extends StatefulWidget {
  final DeviceScreenArguments? args;

  CreateEditDeviceScreen({Key? key, this.args}) : super(key: key);

  @override
  _CreateEditDeviceScreenState createState() => _CreateEditDeviceScreenState();
}

class _CreateEditDeviceScreenState extends State<CreateEditDeviceScreen> {
  final _formKey = GlobalKey<FormState>();
  final _nameController = TextEditingController();
  // Text editing controllers for MQTT configuration
  final TextEditingController _mqttServerController = TextEditingController();
  // final TextEditingController _mqttClientIdController = TextEditingController();
  final TextEditingController _descriptionController = TextEditingController();
  final TextEditingController _portController = TextEditingController();
  final TextEditingController _deviceUsernameController =
      TextEditingController();
  final TextEditingController _devicePasswordController =
      TextEditingController();
  @override
  void initState() {
    super.initState();
    if (widget.args?.device != null) {
      _nameController.text = widget.args?.device['name'] ?? '';
      _mqttServerController.text = widget.args?.device['address'] ?? '';
      _descriptionController.text = widget.args?.device['description'] ?? '';
      _portController.text = widget.args?.device['port'] ?? '';
      _deviceUsernameController.text =
          widget.args?.device['device_username'] ?? '';
      _devicePasswordController.text =
          widget.args?.device['device_password'] ?? '';
    }
  }

  Future<void> _submitDevice() async {
    if (_formKey.currentState?.validate() ?? false) {
      final device = {
        'name': _nameController.text,
        'description': _descriptionController.text,
        'address': _mqttServerController.text,
        'port': _portController.text,
        'device_username': _deviceUsernameController.text,
        'device_password': _devicePasswordController.text,
        'tls': "False",
        "topics": []
      };

      final isEditing = widget.args?.device != null;
      final url = Uri.parse(isEditing
          ? 'http://127.0.0.1:8000/api/devices/${widget.args?.device['id']}/'
          : 'http://127.0.0.1:8000/api/devices/');

      await _sendDeviceRequest(url, device, isEditing);
    }
  }

  Future<void> _sendDeviceRequest(
      Uri url, Map<String, dynamic> device, bool isEditing) async {
    var authProvider = Provider.of<AuthProvider>(context, listen: false);
    var response = isEditing
        ? await http.put(
            url,
            headers: {
              'Content-Type': 'application/json; charset=UTF-8',
              'Authorization': 'Bearer ${authProvider.token}',
            },
            body: json.encode(device),
          )
        : await http.post(
            url,
            headers: {
              'Content-Type': 'application/json; charset=UTF-8',
              'Authorization': 'Bearer ${authProvider.token}',
            },
            body: json.encode(device),
          );

    if (response.statusCode == (isEditing ? 200 : 201)) {
      Navigator.pop(context);
    } else if (response.statusCode == 401) {
      bool tokenRefreshed = await authProvider.refreshAccessToken();
      if (tokenRefreshed && mounted) {
        _sendDeviceRequest(url, device, isEditing);
      }
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
              'Failed to ${isEditing ? 'update' : 'create'} the device. Error: ${response.body}'),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    final isEditing = widget.args?.device != null;

    return Scaffold(
      appBar: AppBar(
        title: Text(isEditing ? 'Edit Device' : 'Create New Device'),
      ),
      body: Form(
        key: _formKey,
        child: ListView(
          padding: const EdgeInsets.all(16.0),
          children: [
            // device name
            TextFormField(
              controller: _nameController,
              decoration: const InputDecoration(labelText: 'Device Name: '),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Please enter a device name';
                }
                return null;
              },
            ),
            // TextFormField(
            //   controller: _mqttServerController,
            //   decoration: const InputDecoration(labelText: 'Broker address: '),
            //   validator: (value) {
            //     if (value == null || value.isEmpty) {
            //       return 'Please enter a broker address';
            //     }
            //     return null;
            //   },
            // ),
            // TextFormField(
            //   controller: _portController,
            //   decoration: const InputDecoration(labelText: 'Broker port: '),
            //   validator: (value) {
            //     if (value == null || value.isEmpty) {
            //       return 'Please enter a broker port';
            //     }
            //     return null;
            //   },
            // ),
            TextFormField(
              controller: _deviceUsernameController,
              decoration: const InputDecoration(labelText: 'Username: '),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Please enter username';
                }
                return null;
              },
            ),
            TextFormField(
              controller: _devicePasswordController,
              decoration: const InputDecoration(labelText: 'Password: '),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Please enter password';
                }
                return null;
              },
            ),
            TextFormField(
              controller: _descriptionController,
              decoration: const InputDecoration(labelText: 'Description: '),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Please enter a description';
                }
                return null;
              },
            ),
            const SizedBox(height: 20),
            ElevatedButton(
              onPressed: _submitDevice,
              child: Text(isEditing ? 'Update Device' : 'Create Device'),
            ),
          ],
        ),
      ),
    );
  }
}
