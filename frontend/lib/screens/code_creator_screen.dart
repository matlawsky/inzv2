import 'package:flutter/material.dart';
import 'package:code_editor/code_editor.dart';
import 'package:frontend/provider/auth_provider.dart';
import 'package:frontend/screens/file_selection_screen.dart';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';
import 'dart:convert';
import 'dart:typed_data';
import 'package:mime/mime.dart';
import 'dart:html'; // Required for Blob
import 'dart:typed_data';
import 'package:frontend/provider/color_state.dart';
import 'package:provider/provider.dart';

class CodeCreatorScreen extends StatefulWidget {
  const CodeCreatorScreen({Key? key}) : super(key: key);

  @override
  State<CodeCreatorScreen> createState() => _CodeCreatorScreenState();
}

class _CodeCreatorScreenState extends State<CodeCreatorScreen> {
  late EditorModel model;
  String _code = ''; // Maintain the current code here
  String? _selectedPort;
  List<String> _ports = [];
  String _statusMessage = "Select a port and run";
  final TextEditingController _fileNameController = TextEditingController();
  String fileContent = '';
  @override
  void initState() {
    super.initState();
    _fetchPorts();
    _initializeEditor();
  }

  void _fetchPorts() async {
    try {
      var authProvider = Provider.of<AuthProvider>(context, listen: false);
      var response = await http.get(
        Uri.parse('http://127.0.0.1:8000/api/serial-ports/'),
        headers: {
          'Authorization': 'Bearer ${authProvider.token}',
        },
      );

      if (response.statusCode == 200) {
        var data = json.decode(response.body);
        if (data['ports'] != null) {
          setState(() {
            _ports = List<String>.from(data['ports']);
            _selectedPort = _ports.isNotEmpty ? _ports.first : null;
          });
        } else {
          print("No 'ports' key in response");
          setState(() {
            _statusMessage = "No 'ports' key in response";
          });
        }
      } else {
        print("Failed to fetch ports. Status code: ${response.statusCode}");
        setState(() {
          _statusMessage = "Failed to fetch ports";
        });
      }
    } catch (e) {
      print("Exception caught while fetching ports: $e");
      setState(() {
        _statusMessage = "Error fetching ports: $e";
      });
    }
  }

  void _initializeEditor() {
    List<String> contentOfFile = [
      "def main():",
      "\tprint('Hello, world!')",
      "",
      "if __name__ == '__main__':",
      "\tmain()",
    ];
    _code = contentOfFile.join("\n");

    List<FileEditor> files = [
      FileEditor(
        language: "python",
        code: _code,
      ),
    ];

    model = EditorModel(
      files: files,
      styleOptions: EditorModelStyleOptions(
        showUndoRedoButtons: false,
        reverseEditAndUndoRedoButtons: false,
      )..defineEditButtonPosition(
          bottom: 10,
          left: 15,
        ),
    );
  }

  Future<void> _sendFile() async {
    if (_selectedPort == null) {
      setState(() {
        _statusMessage = "No port selected";
      });
      return;
    }

    // Assuming _code is updated with the latest code from the editor
    Uint8List codeBytes = utf8.encode(_code);

    // Create a MultipartFile from the bytes
    var file = http.MultipartFile.fromBytes(
      'file',
      codeBytes,
      filename: 'code.py',
      contentType: MediaType('text', 'plain'),
    );

    // Create a multipart request
    var uri = Uri.parse('http://127.0.0.1:8000/api/run-file/');
    var request = http.MultipartRequest('PUT', uri)
      ..files.add(file)
      ..fields['port'] = _selectedPort!;

    var authProvider = Provider.of<AuthProvider>(context, listen: false);
    request.headers['Authorization'] = 'Bearer ${authProvider.token}';

    try {
      var response = await request.send();

      if (response.statusCode == 200) {
        setState(() {
          _statusMessage = "Running on $_selectedPort ...";
        });
      } else {
        print("Failed to send file. Status code: ${response.statusCode}");
        setState(() {
          _statusMessage =
              "Failed to send file. Status code: ${response.statusCode}";
        });
      }
    } catch (e) {
      print("Exception caught while sending file: $e");
      setState(() {
        _statusMessage = "Error: $e";
      });
    }
  }

  Future<void> _fetchFilesystem() async {
    if (_selectedPort == null) {
      setState(() {
        _statusMessage = "No port selected";
      });
      return;
    }
    try {
      var uri = Uri.parse(
          'http://127.0.0.1:8000/api/list-files/?port=$_selectedPort/');

      var authProvider = Provider.of<AuthProvider>(context, listen: false);
      var response = await http.get(
        uri,
        headers: {'Authorization': 'Bearer ${authProvider.token}'},
      );

      // Check if the HTTP request was successful
      if (response.statusCode == 200) {
        var data = json.decode(response.body);
        // Validate the data format
        if (data is Map<String, dynamic> && data.containsKey('filesystem')) {
          List<String> fileList = List<String>.from(data['filesystem']);
          _showFileListDialog(fileList);
        } else {
          // Handle the case where data is not in the expected format
          print("Invalid format in response");
        }
      } else {
        // Handle HTTP error responses
        print(
            "Failed to fetch filesystem data. Status code: ${response.statusCode}");
      }
    } catch (e) {
      // Handle exceptions that occur during the HTTP request
      print("Exception caught while fetching file contents: $e");
    }
  }

  void _showFileListDialog(List<String> fileList) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text("File Contents"),
          content: SingleChildScrollView(
            child: ListBody(
              children:
                  fileList.map((fileContent) => Text(fileContent)).toList(),
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: const Text('Close'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future<String> readPythonFile(String fileName) async {
    var authProvider = Provider.of<AuthProvider>(context,
        listen: false); // Make sure you have AuthProvider

    try {
      var response = await http.get(
        Uri.parse('http://127.0.0.1:8000/api/file-content/$fileName'),
        headers: {'Authorization': 'Bearer ${authProvider.token}'},
      );

      if (response.statusCode == 200) {
        setState(() {
          fileContent = response.body;
        });
      } else {
        print('Failed to load file. Status code: ${response.statusCode}');
      }
    } catch (e) {
      print('Exception caught while reading file: $e');
    }
    return fileContent;
  }

  Future<void> _loadFileContent() async {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => FileSelectionScreen(
          onSelectFile: (String fileName) async {
            // Perform the asynchronous operation outside of setState
            String fileContent = await readPythonFile(fileName);
            // Now update the state synchronously inside setState
            setState(() {
              _code = fileContent;
              model = EditorModel(
                files: [
                  FileEditor(
                    name: fileName,
                    language: "python",
                    code: _code,
                  ),
                ],
              );
            });
          },
        ),
      ),
    );
  }

  Future<String?> _askFileName(BuildContext context) async {
    String? fileName;

    return showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Enter File Name'),
          content: TextField(
            onChanged: (value) => fileName = value.trim(),
            decoration: const InputDecoration(hintText: "File name"),
          ),
          actions: <Widget>[
            TextButton(
              child: const Text('Cancel'),
              onPressed: () => Navigator.of(context).pop(),
            ),
            TextButton(
              child: const Text('Save'),
              onPressed: () {
                Navigator.of(context).pop(fileName);
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> _saveFileToDatabase() async {
    // Call the dialog to ask for the file name
    String? fileName = await _askFileName(context);

    // Check if a file name was provided
    if (fileName == null || fileName.isEmpty) {
      print("No file name provided");
      return;
    }

    // Ensure the file name ends with .py extension
    if (!fileName.endsWith('.py')) {
      fileName += '.py';
    }

    if (fileName.isEmpty) {
      print("No file name provided");
      return;
    }

    var authProvider = Provider.of<AuthProvider>(context, listen: false);

    try {
      // Convert the code into a List<int>
      List<int> codeBytes = utf8.encode(_code);

      // Create a MultipartFile from the bytes with 'text/plain' content type
      var file = http.MultipartFile.fromBytes(
        'file', // Field name for the file
        codeBytes,
        filename: fileName,
        contentType: MediaType('text', 'plain'),
      );

      // Create a multipart request
      var uri = Uri.parse('http://127.0.0.1:8000/api/code_files/');
      var request = http.MultipartRequest('POST', uri)
        ..files.add(file)
        ..headers.addAll({
          'Content-Type': 'multipart/form-data', // Setting content type
          'Authorization': 'Bearer ${authProvider.token}',
        });

      // Send the request
      var response = await request.send();

      if (response.statusCode == 200 || response.statusCode == 201) {
        print("File saved successfully as $fileName");
      } else {
        print("Failed to save file. Status code: ${response.statusCode}");
      }
    } catch (e) {
      print("Exception caught while saving file: $e");
    }
  }

  @override
  Widget build(BuildContext context) {
    final colorState = Provider.of<ColorState>(context);
    return Scaffold(
      appBar: AppBar(
        title: const Text("Code Editor"),
        backgroundColor: colorState.mainColor,
        // actions: [],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            CodeEditor(
                model: model,
                formatters: const ["python"],
                onSubmit: (language, code) {
                  // Updated callback
                  setState(() => _code = code);
                },
                disableNavigationbar: true,
                textModifier: (language, code) {
                  print("A file is about to change");

                  // transform the code before it is saved
                  // if you need to perform some operations on it
                  // like your own auto-formatting for example
                  _code = code;
                  return code;
                }),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                DropdownButton<String>(
                  value: _selectedPort,
                  onChanged: (String? newValue) {
                    setState(() {
                      _selectedPort = newValue;
                    });
                  },
                  items: _ports.map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                ),
                ElevatedButton(
                  onPressed: _fetchFilesystem,
                  child: const Text("Filesystem"),
                ),
                ElevatedButton(
                  onPressed: _sendFile,
                  child: const Text("Run current file"),
                ),
                ElevatedButton(
                  onPressed: _loadFileContent,
                  child: const Text("Load file from DB"),
                ),
                ElevatedButton(
                  onPressed: _saveFileToDatabase,
                  child: const Text("Save file to DB"),
                ),
              ],
            ),
            const SizedBox(height: 20),
            Text(_statusMessage),
            const SizedBox(height: 20),
          ],
        ),
      ),
    );
  }
}
