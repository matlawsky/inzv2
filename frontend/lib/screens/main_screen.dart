import 'package:flutter/material.dart';
import 'package:frontend/screens/login_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'devices_screen.dart';
import 'code_creator_screen.dart';
import 'settings_screen.dart';
import 'package:frontend/provider/color_state.dart';
import 'package:provider/provider.dart';

class MainScreen extends StatefulWidget {
  @override
  MainScreenState createState() => MainScreenState();
}

class MainScreenState extends State<MainScreen> {
  // Tracks the currently selected screen
  Widget _currentScreen = DevicesScreen();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    var screenWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: const Text('KONCEPTO'),
        leading: screenWidth < 600 // Arbitrary breakpoint for smaller screens
            ? IconButton(
                icon: const Icon(Icons.menu),
                onPressed: () => _scaffoldKey.currentState!.openDrawer(),
              )
            : null,
      ),
      drawer: screenWidth >= 600 ? null : _buildDrawer(),
      body: screenWidth >= 600
          ? Row(
              children: [
                Expanded(
                  flex: 2,
                  child:
                      _buildDrawer(), // Always visible drawer on larger screens
                ),
                Expanded(
                  flex: 8,
                  child: _currentScreen,
                ),
              ],
            )
          : _currentScreen,
    );
  }

  Widget _buildDrawer() {
    final colorState = Provider.of<ColorState>(context);

    return Drawer(
      child: Column(
        children: [
          Expanded(
            child: ListView(
              padding: EdgeInsets.zero,
              children: <Widget>[
                DrawerHeader(
                  decoration: BoxDecoration(
                    color: colorState.mainColor,
                  ),
                  child: const Text('Menu',
                      style: TextStyle(color: Colors.white, fontSize: 25)),
                ),
                ListTile(
                  title: const Text('Devices'),
                  onTap: () {
                    _updateScreen(DevicesScreen());
                  },
                ),
                ListTile(
                  title: const Text('Code'),
                  onTap: () {
                    _updateScreen(const CodeCreatorScreen());
                  },
                ),
                ListTile(
                  title: const Text('Settings'),
                  onTap: () {
                    _updateScreen(SettingsScreen());
                  },
                ),
              ],
            ),
          ),
          // Log Out Button
          Container(
            padding: const EdgeInsets.all(8.0),
            child: Align(
              alignment: FractionalOffset.bottomCenter,
              child: ListTile(
                leading: const Icon(Icons.exit_to_app),
                title: const Text('Log Out'),
                onTap: () async {
                  final prefs = await SharedPreferences.getInstance();
                  await prefs.clear();
                  Navigator.of(context).pushAndRemoveUntil(
                    MaterialPageRoute(builder: (context) => LoginScreen()),
                    (Route<dynamic> route) => false,
                  );
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _updateScreen(Widget screen) {
    setState(() {
      _currentScreen = screen;
    });
    if (MediaQuery.of(context).size.width < 600) {
      Navigator.of(context).pop(); // Close the drawer on smaller screens
    }
  }
}
