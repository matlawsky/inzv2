import 'package:flutter/material.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:frontend/provider/auth_provider.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:provider/provider.dart';
import 'package:frontend/provider/color_state.dart'; // Ensure this import points to your ColorState file

class SettingsScreen extends StatefulWidget {
  @override
  SettingsScreenState createState() => SettingsScreenState();
}

class SettingsScreenState extends State<SettingsScreen> {
  final TextEditingController _passwordController = TextEditingController();
  final int timeout = 30; // Default timeout value
  final int keepAlive = 10; // Default keep-alive interval

  Future<void> _changePassword() async {
    try {
      var authProvider = Provider.of<AuthProvider>(context, listen: false);

      // Make the API call using the token
      var response = await http.post(
        Uri.parse('http://127.0.0.1:8000/api/change-password'),
        headers: {
          "Content-Type": "application/json",
          'Authorization': 'Bearer ${authProvider.token}',
        },
        body: json.encode({'password': _passwordController.text}),
      );

      if (response.statusCode == 200) {
        // Handle successful response
      } else if (response.statusCode == 401) {
        // If unauthorized, attempt token refresh
        bool tokenRefreshed = await authProvider.refreshAccessToken();

        if (tokenRefreshed && mounted) {
          // If token is refreshed and the widget is still in the tree, retry changing password
          _changePassword();
        } else {
          // Handle failed token refresh or unmounted widget
          // Consider navigating to the login screen or showing an error message
        }
      } else {
        // Handle other error responses
      }
    } catch (e, stacktrace) {
      print('Exception caught: $e');
      print('Stacktrace: $stacktrace');
      // Add additional error handling as needed
    }
  }

  @override
  Widget build(BuildContext context) {
    final colorState = Provider.of<ColorState>(context);

    return Scaffold(
      appBar: AppBar(
        title: const Text("Settings"),
        backgroundColor: colorState.mainColor,
      ),
      body: ListView(
        padding: const EdgeInsets.all(16),
        children: [
          const Text('Change Password',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
          const SizedBox(height: 8),
          TextFormField(
            controller: _passwordController,
            decoration: const InputDecoration(
              labelText: 'New Password',
              border: OutlineInputBorder(),
            ),
            obscureText: true,
          ),
          const SizedBox(height: 20),
          ElevatedButton(
            onPressed: _changePassword,
            child: const Text('Update Password'),
          ),
          const SizedBox(height: 20),

          // Theme Section
          const Padding(
            padding: EdgeInsets.symmetric(vertical: 8.0),
            child: Text(
              'Theme',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
          ),
          _buildColorPickerTile(
            'Main Color',
            colorState.mainColor,
            (color) => colorState.setMainColor(color),
          ),
          _buildColorPickerTile(
            'Background Color',
            colorState.backgroundColor,
            (color) => colorState.setBackgroundColor(color),
          ),
          _buildColorPickerTile(
            'Font Color',
            colorState.fontColor,
            (color) => colorState.setFontColor(color),
          ),
        ],
      ),
    );
  }

  Widget _buildColorPickerTile(
      String title, Color currentColor, Function(Color) onColorChanged) {
    return ListTile(
      title: Text(title),
      trailing: Container(
        width: 30,
        height: 30,
        color: currentColor,
      ),
      onTap: () {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('Select a $title'),
              content: SingleChildScrollView(
                child: ColorPicker(
                  pickerColor: currentColor,
                  onColorChanged: (color) =>
                      setState(() => onColorChanged(color)),
                ),
              ),
              actions: <Widget>[
                ElevatedButton(
                  child: const Text('Done'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
      },
    );
  }

  @override
  void dispose() {
    _passwordController.dispose();
    super.dispose();
  }
}
