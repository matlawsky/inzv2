import 'package:flutter/material.dart';

class ColorState with ChangeNotifier {
  Color _mainColor = Colors.blue;
  Color _backgroundColor = Colors.white; // Default background color
  Color _fontColor = Colors.black; // Default font color

  Color get mainColor => _mainColor;
  Color get backgroundColor => _backgroundColor;
  Color get fontColor => _fontColor;

  void setMainColor(Color color) {
    _mainColor = color;
    notifyListeners();
  }

  void setBackgroundColor(Color color) {
    _backgroundColor = color;
    notifyListeners();
  }

  void setFontColor(Color color) {
    _fontColor = color;
    notifyListeners();
  }
}
