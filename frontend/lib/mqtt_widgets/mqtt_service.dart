import 'package:flutter/foundation.dart';
import 'package:mqtt_client/mqtt_client.dart' as mqtt;
import 'package:mqtt_client/mqtt_browser_client.dart' as mqtt;

class MQTTManager with ChangeNotifier {
  mqtt.MqttBrowserClient? _client;
  late String topic; // Define topic as a member variable

  List<List<dynamic>> _messages = [];

  List<List<dynamic>> get messages => _messages;

  Future<void> connect(String server, String topic) async {
    _client = mqtt.MqttBrowserClient(server, 'flutter_client');
    _client!.onConnected = _onConnected;
    _client!.onDisconnected = _onDisconnected;
    _client!.onSubscribed = (String t) => _onSubscribed(t, topic);

    try {
      await _client!.connect();
    } catch (e) {
      print('Error: $e');
    }
  }

  void _onConnected() {
    print('Connected to MQTT');
    _client!.subscribe(topic, mqtt.MqttQos.atMostOnce);
  }

  void _onDisconnected() {
    print('Disconnected from MQTT');
  }

  void _onSubscribed(String t, String topic) {
    print('Subscribed to topic: $topic');
    _client!.updates!
        .listen((List<mqtt.MqttReceivedMessage<mqtt.MqttMessage>> c) {
      final mqtt.MqttPublishMessage recMess =
          c[0].payload as mqtt.MqttPublishMessage;
      final String message = mqtt.MqttPublishPayload.bytesToStringAsString(
          recMess.payload.message);

      _messages.add([DateTime.now().toString(), message]);
      notifyListeners();
    });
  }

  @override
  void dispose() {
    _client?.disconnect();
    super.dispose();
  }
}
